package com.belhard.weapon.service;

import com.belhard.weapon.entity.Army;
import com.belhard.weapon.entity.Soldier;

/**
 * Created by Владимир on 09.01.2017.
 */
public class ArmyService {
    public static int calcArmyWeaponPrice(Army army) {
        int result = 0;
        for (Soldier s : army.getSoldiers()) {
            result += s.getWeapon().getPrice();
        }
        return result;
    }

    public static int calcArmyWeaponWeight(Army army) {
        int result = 0;
        for (Soldier s : army.getSoldiers()) {
            result += s.getWeapon().getWeight();
        }
        return result;
    }
}
