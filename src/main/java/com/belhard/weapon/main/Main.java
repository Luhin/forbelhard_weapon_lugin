package com.belhard.weapon.main;

import com.belhard.weapon.entity.Army;
import com.belhard.weapon.entity.Gun;
import com.belhard.weapon.entity.Katana;
import com.belhard.weapon.entity.Soldier;
import com.belhard.weapon.service.ArmyService;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Владимир on 09.01.2017.
 */
public class Main {
    public static void main(String[] args) {
        List<Soldier> soldiers = Arrays.asList(
                new Soldier(new Katana(100, 3)), new Soldier(new Gun(60, 10))
                , new Soldier(new Katana(200, 1)), new Soldier(new Gun(94, 7))
                , new Soldier(new Katana(400, 3)), new Soldier(new Gun(55, 11))
                , new Soldier(new Katana(200, 3)), new Soldier(new Gun(70, 13))
                , new Soldier(new Katana(140, 3)), new Soldier(new Gun(80, 11)));

        Army army = new Army(soldiers);
        System.out.println("All weapon price : " + ArmyService.calcArmyWeaponPrice(army) + "$");
        System.out.println("All weapon weight : " + ArmyService.calcArmyWeaponWeight(army) + "kg");
        army.attack();
    }
}
