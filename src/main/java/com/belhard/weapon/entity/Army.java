package com.belhard.weapon.entity;

import java.util.List;

/**
 * Created by Владимир on 09.01.2017.
 */
public class Army {
    private List<Soldier> soldiers;

    public Army(List<Soldier> soldiers) {
        this.soldiers = soldiers;
    }

    public void attack() {
        soldiers.stream().forEach(s -> s.attack());
    }

    public List<Soldier> getSoldiers() {
        return soldiers;
    }

}
