package com.belhard.weapon.entity;

/**
 * Created by Владимир on 09.01.2017.
 */
public class Katana extends Weapon {

    public Katana(int price, int weight) {
        super(price, weight);
    }

    public void attack() {
        System.out.println("Attack with Katana");
    }
}
