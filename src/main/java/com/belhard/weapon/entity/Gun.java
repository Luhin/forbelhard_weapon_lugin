package com.belhard.weapon.entity;

/**
 * Created by Владимир on 09.01.2017.
 */
public class Gun extends Weapon {

    public Gun(int price, int weight) {
        super(price, weight);
    }

    public void attack() {
        System.out.println("Shooting with the Gun");
    }
}
