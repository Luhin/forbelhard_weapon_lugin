package com.belhard.weapon.entity;

/**
 * Created by Владимир on 09.01.2017.
 */
public class Soldier {
    private Weapon weapon;

    public Soldier(Weapon weapon) {
        this.weapon = weapon;
    }

    public void attack() {
        this.weapon.attack();
    }

    public Weapon getWeapon() {
        return weapon;
    }
}
