package com.belhard.weapon.entity;

/**
 * Created by Владимир on 09.01.2017.
 */
public abstract class Weapon {
    private int price;
    private int weight;
    public Weapon(int price, int weight){
        this.price = price;
        this.weight = weight;
    }

    public int getPrice() {
        return price;
    }

    public int getWeight() {
        return weight;
    }

    public abstract void attack();
}
